package com.dad.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DadGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DadGatewayApplication.class, args);
    }

}
