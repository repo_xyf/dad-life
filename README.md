# 点滴[Dribs and drabs]
## 平台简介
立意：记录生活的点点滴滴~
## 目录结构
~~~text
dad
├── dad-ui              // 前端框架 [80]
├── dad-gateway         // 网关模块 [8080]
├── dad-auth            // 认证中心 [9200]
├── dad-api             // 接口模块
│       └── dad-api-system                          // 系统接口
├── dad-common          // 通用模块
│       └── dad-common-core                         // 核心模块
│       └── dad-common-datascope                    // 权限范围
│       └── dad-common-datasource                   // 多数据源
│       └── dad-common-log                          // 日志记录
│       └── dad-common-redis                        // 缓存服务
│       └── dad-common-security                     // 安全模块
│       └── dad-common-swagger                      // 系统接口
├── dad-modules         // 业务模块
│       └── dad-system                              // 系统模块 [9201]
│       └── dad-gen                                 // 代码生成 [9202]
│       └── dad-job                                 // 定时任务 [9203]
│       └── dad-file                                // 文件服务 [9300]
├── dad-visual          // 图形化管理模块
│       └── dad-visual-monitor                      // 监控中心 [9100]
## 功能介绍
