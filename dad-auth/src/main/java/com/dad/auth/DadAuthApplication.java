package com.dad.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DadAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(DadAuthApplication.class, args);
    }

}
